# wylistowanie kategorii
curl localhost:5000/categories

# dodanie nowej kategorii
curl -H "Content-Type: application/json" --data '{"category": "foo"}' localhost:5000/categories 

# usuniecie kategorii
curl -XDELETE -H "Content-Type: application/json" --data '{"category": "foo"}' localhost:5000/categories 

# wylistowanie przedmiotow w kategorii
curl localhost:5000/categories/koczkodany

# dodanie nowego przedmiotu do kategorii
curl -H "Content-Type: application/json" --data '{"item": "kaczka"}' localhost:5000/categories/koczkodany

# usuniecie przedmiotu z kategorii
curl -XDELETE -H "Content-Type: application/json" --data '{"item": "kaczka"}' localhost:5000/categories/koczkodany

