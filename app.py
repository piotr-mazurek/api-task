from flask import Flask, send_from_directory
import json
from flask import request

app = Flask(__name__, static_url_path='')
app.debug = True


categories = {
    'koczkodany': []
}


@app.route('/categories')
def list_cats():
    return json.dumps({
        'categories': list(categories.keys())
    })


@app.route('/categories', methods=["POST"])
def add_cat():
    try:
        cat = request.json['category']
        if cat not in categories:
            categories[cat] = []
        return json.dumps({
            'categories': list(categories.keys())
        })
    except:
        return json.dumps({'error': 'Something went wrong'}), 500


@app.route('/categories', methods=["DELETE"])
def delete_cat():
    try:
        cat = request.json['category']
        del categories[cat]
        return json.dumps({
            'categories': list(categories.keys())
        })
    except:
        return json.dumps({'error': 'Something went wrong'}), 500


@app.route('/categories/<cat_id>')
def list_items(cat_id):
    try:
        return json.dumps(categories[cat_id])
    except:
        return json.dumps({'error': 'Something went wrong'}), 500


@app.route('/categories/<cat_id>', methods=['POST'])
def add_item(cat_id):
    try:
        item = request.json['item']
        categories[cat_id].append(item)
        return json.dumps(categories[cat_id])
    except:
        return json.dumps({'error': 'Something went wrong'}), 500


@app.route('/categories/<cat_id>', methods=['DELETE'])
def delete_item(cat_id):
    try:
        item = request.json['item']
        categories[cat_id].remove(item)
        return json.dumps(categories[cat_id])
    except:
        return json.dumps({'error': 'Something went wrong'}), 500


@app.route('/<path:path>')
def send_files(path):
    return send_from_directory('static', path)


app.run()
